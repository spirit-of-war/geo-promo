<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

class GeoController extends Controller
{
    const TIME_GEO_CACHE = 60*30;

    public function actionIp2geo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cache = Yii::$app->cache;
        $ip = Yii::$app->request->get('ip');

        if ($ip && $this->is_valid_ip($ip)) {
            if ($info = $cache->get($ip)) {
                $geoInfo = $info;
            } else {
                $geoInfo = Yii::$app->geoip->ip($ip);
            }

            if ($geoInfo && $geoInfo->country) {
                $cache->set($ip, $geoInfo, self::TIME_GEO_CACHE);

                return [
                    'city' => $geoInfo->city,
                    'country' => $geoInfo->country,
                    'longitude' => $geoInfo->location->lng,
                    'latitude' => $geoInfo->location->lat
                ];
            }
        }

        return [
            'error' => '404'
        ];

    }

    private function is_valid_ip($ip) {
        $ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
        $ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';
        return preg_match("/^($ipv4|$ipv6)\$/", trim($ip));
    }
}